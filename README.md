# Solar Data

This is a *mostly* complete set of data from some residential solar panels in London, UK.

It updates daily at sunset.

These data power the graphs at [@edent_solar on Twitter](https://twitter.com/edent_solar) and <a rel="me" href="https://botsin.space/@solar">@solar @ BotsIn.Space on Mastodon</a>. You can [read more about these East/West split panels on my blog](https://shkspr.mobi/blog/2021/03/which-generates-more-electricity-east-or-west-facing-solar-panels/).

## Location and orientation

The location of the panels is *roughly* latitude 51.486, longitude 0.107. Orientation is almost exactly 90°.

## Technology

Each side of the roof has 8 panels. They are SolarWatt ECO 60M 315Wp.  That means each side has a theoretical maximum of 2.52kWp.  So the roof's total is 5.04kWp.

You can [read about the panels' installation](https://shkspr.mobi/blog/2020/03/relaunching-edent_solar-part-1-installation/).

The inverter is a Fronius Primo 3.6.1.

You can [read my blog post about the inverter](https://shkspr.mobi/blog/2020/03/relaunching-edent_solar-part-2-inverter/).

## Data Format

The data are saved in daily files, named in ISO8601 format `YYYY-MM-DD`

Files are CSV format with no header.  The columns are:

> 24 Hours Timestamp, West-facing panels (Watts), East-facing panels (Watts)

For example:

```
5:40,0,0
5:45,0,0
5:50,6,13
5:55,4,9
6:00,7,16
6:05,8,18
6:10,15,79
6:15,11,151
6:20,14,208
6:25,17,260
6:30,51,308
6:35,75,285
...
12:40,1455,1599
12:45,1480,1601
12:50,1490,1583
12:55,1503,1573
13:00,1516,1556
13:05,1533,1541
13:10,1541,1517
13:15,1566,1512
13:20,1572,1492
13:25,1593,1487
13:30,1598,1463
13:35,1609,1450
...
18:40,42,65
18:45,23,61
18:50,23,50
18:55,18,19
19:00,19,25
19:05,16,54
19:10,17,37
19:15,16,36
19:20,14,17
19:25,16,19
19:30,11,14
19:35,15,17
...
```

All files run from 04:30 to 21:30 which should encompass all daylight hours in London, UK.

## Errata

These data are sourced directed from the solar inverter using the [Fronius API](https://www.fronius.com/en/solar-energy/installers-partners/technical-data/all-products/system-monitoring/open-interfaces/fronius-solar-api-json-). I assume that the data from there are broadly accurate. They tally with what my generation meter displays.

On some days, the network connection to the inverter was disrupted. On those occasions, no data was recorded for a specific day.

## Licence

These data are licensed to you as [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). If you use them in an academic paper, you are expected to publish as open access.
