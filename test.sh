#!/bin/bash

# Set the date limit
date_limit="2023-08-01"

# Initialize the total sum variable
total_sum=0

# Loop through each CSV file that matches the date criteria
for file in *.csv; do
    # Extract the date part from the filename
    file_date=$(basename "$file" .csv)
    
    # Check if the file date is greater than the date limit
    if [[ "$file_date" > "$date_limit" ]]; then
        # Sum the second column of the current file, excluding the header
        file_sum=$(awk -F, 'NR > 1 {sum += $2} END {print sum}' "$file")
        
        # Add the file sum to the total sum
        total_sum=$(echo "$total_sum + $file_sum" | bc)
    fi
done

# Print the total sum
echo "Total sum of the 2nd column after $date_limit: $total_sum"
